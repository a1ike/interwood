"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* function hideAllError(str) { return true; }
window.onerror = hideAllError; */
$(document).ready(function () {
  var _Swiper;

  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });
  $('.i-header__toggle').on('click', function (e) {
    e.preventDefault();
    $('.i-header__row').slideToggle('fast');
    $('.i-header__nav').slideToggle('fast');
  });
  $('.i-tech-dot__icon').on('click', function (e) {
    e.preventDefault();
    $('.i-tech-dot__info').hide();
    $(this).next().toggle();
  });
  $('.i-tech-dot__info').on('click', function (e) {
    e.preventDefault();
    $(this).toggle();
  });
  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.i-modal').toggle();
  });
  $('.i-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'i-modal__centered') {
      $('.i-modal').hide();
    }
  });
  $('.i-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.i-modal').hide();
  });
  $('.i-tabs__header li').click(function () {
    var tab_id = $(this).attr('data-tab');
    $('.i-tabs__header li').removeClass('current');
    $('.i-tabs__content').removeClass('current');
    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });
  new Swiper('.i-top', {
    pagination: {
      el: '.i-top .swiper-pagination'
    },
    autoplay: {
      delay: 4000
    },
    on: {
      init: function init() {
        $('.swiper-pagination-total').html(this.slides.length);
        $('.swiper-pagination-current').html(++this.realIndex);
      },
      slideChange: function slideChange() {
        $('.swiper-pagination-current').html(++this.realIndex);
      }
    }
  });
  new Swiper('.i-home-projects__cards', {
    spaceBetween: 100,
    pagination: {
      el: '.i-home-projects__cards .swiper-pagination'
    },
    navigation: {
      nextEl: '.i-home-projects__cards .swiper-button-next',
      prevEl: '.i-home-projects__cards .swiper-button-prev'
    },
    allowTouchMove: false
  });
  $('.gallery-top').each(function (index, element) {
    var $this = 's' + index;
    $this = new Swiper('#s' + index, {
      spaceBetween: 13,
      slidesPerView: 'auto',
      freeMode: false,
      watchSlidesVisibility: true,
      watchSlidesProgress: true
    });
    var $this2 = 'b' + index;
    $this2 = new Swiper('#b' + index, {
      spaceBetween: 10,
      thumbs: {
        swiper: $this
      }
    });
  });
  new Swiper('.i-home-reviews__cards', {
    spaceBetween: 100,
    loop: true,
    navigation: {
      nextEl: '.i-home-reviews__cards .swiper-button-next',
      prevEl: '.i-home-reviews__cards .swiper-button-prev'
    },
    allowTouchMove: false
  });
  new Swiper('.i-photos__cards', {
    spaceBetween: 100,
    pagination: {
      el: '.i-photos__cards .swiper-pagination'
    },
    navigation: {
      nextEl: '.i-photos__cards .swiper-button-next',
      prevEl: '.i-photos__cards .swiper-button-prev'
    }
  });
  new Swiper('.i-gallery__cards', (_Swiper = {
    spaceBetween: 100,
    pagination: {
      el: '.i-gallery__cards .swiper-pagination'
    },
    navigation: {
      nextEl: '.i-gallery__cards .swiper-button-next',
      prevEl: '.i-gallery__cards .swiper-button-prev'
    }
  }, _defineProperty(_Swiper, "spaceBetween", 45), _defineProperty(_Swiper, "breakpoints", {
    1024: {
      slidesPerView: 2
    },
    1200: {
      slidesPerView: 3
    }
  }), _Swiper));
  $('input[name=\'type_switch\']').change(function () {
    $('#mount').toggleClass('i-calc_disabled');
  });
  $('input[name=\'calc_what\']').change(function () {
    if ($('input[name=\'calc_what\']:checked').attr('id') === 'calc_what_1') {
      $('.i-calc__choose').hide();
      $('#calc_choose_1').show();
    }

    if ($('input[name=\'calc_what\']:checked').attr('id') === 'calc_what_2') {
      $('.i-calc__choose').hide();
      $('#calc_choose_2').show();
    }

    if ($('input[name=\'calc_what\']:checked').attr('id') === 'calc_what_3') {
      $('.i-calc__choose').hide();
      $('#calc_choose_3').show();
    }

    if ($('input[name=\'calc_what\']:checked').attr('id') === 'calc_what_4') {
      $('.i-calc__choose').hide();
      $('#calc_choose_4').show();
    }
  });
  $('.i-breadcrumps').detach().appendTo('.i-header');
  var slider = document.getElementById('calc_height');
  noUiSlider.create(slider, {
    start: 1000,
    connect: [true, false],
    step: 10,
    range: {
      'min': 600,
      'max': 2600
    }
  }, true);
  slider.noUiSlider.on('update', function (values, handle) {
    $('#calc_input_height').val(Math.round(values[0]));
    $('#calc_height .noUi-touch-area').attr('data-before', Math.round(values[0]) + ' мм');
  });
  $('#calc_input_height').on('input', function () {
    slider.noUiSlider.set([$(this).val(), null]);
  });
  var slider2 = document.getElementById('calc_width');
  noUiSlider.create(slider2, {
    start: 1000,
    connect: [true, false],
    step: 10,
    range: {
      'min': 600,
      'max': 2600
    }
  }, true);
  slider2.noUiSlider.on('update', function (values, handle) {
    $('#calc_input_width').val(Math.round(values[0]));
    $('#calc_width .noUi-touch-area').attr('data-before', Math.round(values[0]) + ' мм');
  });
  $('#calc_input_width').on('input', function () {
    slider2.noUiSlider.set([$(this).val(), null]);
  });
});
//# sourceMappingURL=main.js.map
